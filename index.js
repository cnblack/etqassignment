/////////////misc functions
/**
 * recursively traverse the reply tree to get all the relpys and convert
 * them to an instance of the Comment class
 *
 * @param {Object} obj - the reply object.
 * @param {App} app - the main app with the database to get a refrence to the app.
 */
const convertToComment = (obj, app) => {
    if (obj.length > 0 || !obj instanceof Comment) {
        for (let i = 0; i < obj.length;i ++) {
            obj[i] = new Comment(obj[i].id, obj[i].userId, obj[i].body, obj[i].replys, app);
            convertToComment(obj[i]);
        }
    } else {
        obj = new Comment(obj.id, obj.userId, obj.body, obj.replys, app);
    }
}

/**
 * for when you save the database this deletes the app refrence from the reply objects
 * recursively
 *
 * @param {Object} obj - the reply object.
 * @param {App} app - the main app with the database to get a refrence to the app.
 */
const deleteAppObject = (obj) => {
    if (obj.length > 0 || obj.app) {
        for (let i = 0; i < obj.length;i ++) {
            delete obj[i].app;
            convertToComment(obj[i]);
        }
    } else {
        delete obj.app;
    }

    if (obj.app)
    delete obj.app;
}

//classes 
class App {
    constructor() {
        this.database = {};
    }
    /**
     * creates a new user
     *
     * @param {Number} id - The ID of the User.
     * @param {String} email - The user's email.
     * @param {String} userName - The user's username.
     */
    createUser(id, email, userName) {
        if (!this.database.users) 
            this.database.users = {};
        if (this.userExists(id))
            return console.error('user already exists');
        this.database.users[id] = new User(id, email, userName, [], {}, this);
        return this.database.users[id];
    }
    /**
     * removes user by ID
     *
     * @param {Number} id - The ID of the User.
     */
    removeUser(id) {
        if (this.userExists(id))
            delete this.database.users[id];
        else console.error("can't remove a user that doesn't exist");
    }
    /**
     * get all users
     *
     */
    getUsers() {
        if (this.database.users)
            return this.database.users;
        else {
            console.error('no users');
            return undefined;
        }
    }
    /**
     * get user by ID
     *
     * @param {Number} id - The ID of the User.
     */
    getUser(id) {
        if (this.database.users && this.database.users[id])
            return this.database.users[id];
        else {
            console.error('no such user exists');
            return undefined;
        }
    }
    /**
     * check if user exists by it's ID
     *
     * @param {Number} id - The ID of the User.
     */
    userExists(id) {
        return this.database.users[id] !== undefined;
    }
    /**
     * read the database 
     *
     * @param {String} filename - the database filename.
     */
    readDatabase(filename) {
        const self = this;
        return new Promise((resolve, reject) => {
            fetch(filename)
            .then(result => result.json())
            .then(result => {
                
                let database = {};

                const {users} = result;

                for (let _user in users) {
                    let user = users[_user];
                    const posts = user.posts;
                    for (let _post in posts) {
                        let post = posts[_post];
                        const comments = post.comments;
                        for (let i = 0; i < comments.length; i ++) {
                            let comment = comments[i];
                            const replys = comment.replys;
                            convertToComment(replys, self);
                            comments[i] = new Comment(comment.id, comment.userId, comment.body, replys, self);
                            //console.log(replys);                            
                        }
                        posts[_post] = new Post(post.id, post.body, comments, self);
                        //console.log(comments);
                    }
                    users[_user] = new User(user.id, user.email, user.userName, user.friends, posts, self);
                }

                

                self.database = {
                    users: users
                };

                resolve(self.database);

            })
            .catch(err => reject(err));
        })
        
    }
    /**
     * save the database to a file
     *
     */
    saveDatabase() {
        //this is the best way to save files localy using javascript
        //cause it can't access files locally and read/write them
        const {users} = this.database;
        for (let _user in users) {
            let user = users[_user];
            const posts = user.posts;
            for (let _post in posts) {
                let post = posts[_post];
                const comments = post.comments;
                for (let i = 0; i < comments.length; i ++) {
                    let comment = comments[i];
                    const replys = comment.replys;
                    deleteAppObject(replys, self);
                    delete comments[i].app;
                    //console.log(replys);                            
                }
                delete posts[_post].app;
                //console.log(comments);
            }
            delete users[_user].app;
        }
        console.log(this.database);

        const a = document.createElement('a');
        a.setAttribute('href', 'data:text/plain;charset=utf-u,'+encodeURIComponent(JSON.stringify(this.database)));
        a.setAttribute('download', 'SocialDatabase.json');
        a.click();
    }
}



class User {
    constructor(id, email, userName, friends = [], posts = {}, app) {
        this.id = id;
        this.email = email;
        this.userName = userName;
        this.friends = friends;
        this.posts = posts;
        //console.log(id,app);
        this.app = app;
    }
    /**
     * get post by ID
     *
     * @param {Number} id - The ID of the Post.
     */
    getPost(id) {
        if (this.posts[id] === undefined) {
            console.error("no post exists");
            return undefined;
        }
        return this.posts[id];
    }
    /**
     * check if Post exists by it's ID
     *
     * @param {Number} id - The ID of the Post.
     */
    postExists(id) {
        return this.posts[id] !== undefined
    }

    /**
     * add a post to the current user
     *
     * @param {Number} id - The ID of the Post.
     * @param {String} body - The post's body/content.
     */
    createPost(id, body) {
        if (id === undefined || body === undefined)
            return console.error("can't add post with no ID or body");
        if (this.postExists(id))
            return console.error("there is already a post with this ID");
        this.posts[id] = new Post(id, body, [], this.app);
        return this.posts[id];
    }
    /**
     * removes post by ID
     *
     * @param {Number} id - The ID of the Post.
     */
    removePost(id) {
        if (this.postExists(id))
            delete this.posts[id];
        console.error("can't remove a post that doesn't exist");
    }
}

class Post {
    constructor(id, body, comments = [], app) {
        this.id = id;
        this.body = body;
        this.comments = comments;
        this.app = app;
    }
    /**
     * get Comment by ID
     *
     * @param {Number} id - The ID of the Comment.
     */
    getComment(id) {
        if (!this.commentExists(id)) {
            console.error("no comment exists");
            return undefined;
        }
        return this.comments.filter(el => el.id === id)[0];
    }
    /**
     * check if Comment exists by it's ID
     *
     * @param {Number} id - The ID of the Comment.
     */
    commentExists(id) {
        return this.comments.some((el) => el.id === id)
    }

    /**
     * add a comment to the current post
     *
     * @param {Number} id - The ID of the Comment.
     * @param {Number} userId - The ID of the user who made the comment.
     * @param {String} body - The comments's body/content.
     */
    createComment(id, userId, body) {
        if (id === undefined || body === undefined || userId === undefined)
            return console.error("can't add comment with no ID or body or userId");
        if (this.commentExists(id))
            return console.error("there is already a comment with this ID");
        if (!this.app.userExists(userId))
            return console.error("no user with such id");
        const comment = new Comment(id, userId, body, [], this.app)
            this.comments.push(comment);
        return comment
    }
    /**
     * removes Comment by ID
     *
     * @param {Number} id - The ID of the Comment.
     */
    removeComment(id) {
        if (this.commentExists(id))
            this.comments.splice(this.comments.indexOf(this.getComment(id)), 1);
        console.error("can't remove a comment that doesn't exist");
    }
}


//Comment class will be used for both comments and relpys
class Comment {
    constructor(id, userId, body, replys = [], app) {
        this.id = id;
        this.userId = userId;
        this.body = body;
        this.replys = replys;
        this.app = app;
    }
    /**
     * get Reply by ID
     *
     * @param {Number} id - The ID of the Reply.
     */
    getReply(id) {
        if (!this.replyExists(id)) {
            console.error("no reply exists");
            return undefined;
        }
        return this.replys.filter(el => el.id === id)[0];
    }
    /**
     * check if Reply exists by it's ID
     *
     * @param {Number} id - The ID of the Reply.
     */
    replyExists(id) {
        return this.replys.some((el) => el.id === id)
    }

    /**
     * add a reply to the current post
     *
     * @param {Number} id - The ID of the reply.
     * @param {Number} userId - The ID of the user who made the reply.
     * @param {String} body - The replys's body/content.
     */
    createReply(id, userId, body) {
        if (id === undefined || body === undefined || userId === undefined)
            return console.error("can't add reply with no ID or body or userId");
        if (this.replyExists(id))
            return console.error("there is already a reply with this ID");
        if (!this.app.userExists(userId))
            return console.error("no user with such id");

        const reply = new Comment(id, userId, body, [], this.app)
        this.replys.push(reply);
        return reply;
    }
    /**
     * removes Reply by ID
     *
     * @param {Number} id - The ID of the Reply.
     */
    removeReply(id) {
        if (this.replyExists(id))
            this.replys.splice(this.replys.indexOf(this.getReply(id)), 1);
        console.error("can't remove a reply that doesn't exist");
    }
}





let app;
///////////////main testing operations
const testingOperations = () => {
    ////////chaining operations
    ////users
    app.createUser(4, 'hazem.huzayen@gmail.com', 'Hazem');
    app.createUser(5, 'yara.huzayen@gmail.com', 'Yara');
    app.createUser(5, 'yara.huzayen@gmail.com', 'Yara'); //this won't work
    info.innerHTML += '<br> added 2 users Hazem, Yara';
                
    ////posts
    app.getUser(4).createPost(1, "Hello!");
    app.getUser(4).createPost(); //this won't work
    app.getUser(4).createPost(1, "Hi"); //this won't work
    info.innerHTML += '<br> added a post to Hazem with the body Hello';


    ////comments
    app.getUser(4).getPost(1).createComment(1, 1, "hey dude!");
    app.getUser(4).getPost(1).createComment(1, 1, "hey dude!");//this won't work
    app.getUser(4).getPost(1).createComment(2, 3, "hey dude!");
    app.getUser(4).getPost(1).createComment(3, 12, "hey dude!");//this won't work   
    info.innerHTML += '<br> added 2 comments to the Post';
    
    ////replys
    app.getUser(4).getPost(1).getComment(1).createReply(1, 2, "what's up");
    app.getUser(4).getPost(1).getComment(1).createReply(1, 2, "what's up");//wont work
    app.getUser(4).getPost(1).getComment(1).createReply(11, 20, "what's up");//wont work
    info.innerHTML += '<br> added 1 replys to the first comment';
    

    ////////using variables
    let user = app.getUser(4);
    let post = user.getPost(1);
    let comment = post.getComment(1);
    let reply1 = comment.getReply(1);

    console.log(app.getUsers());
}

const saveJSON = () => {
    if (app) {
        app.saveDatabase();
        info.innerHTML += '<br> saving database';
        setTimeout(() => {
            app = undefined;
            info.innerHTML += '<br> load new database';
        }, 500);
    }
}

const loadJSON = (filename) => {
    app = new App();
    app.readDatabase(filename)
        .then(result => {
            
            info.innerHTML += "<br>Done loading";
            testingOperations();
        })
        .catch(err => console.error(err));
}

loadJSON('SocialDatabase.json');